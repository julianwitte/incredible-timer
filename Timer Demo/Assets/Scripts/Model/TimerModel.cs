﻿using UnityEngine;
using System.Collections;

public class TimerModel {

    public float ElapsedTime { get; private set; }
    public delegate void IncrementedTime(float elapsedTime);
    public event IncrementedTime OnIncrementedTime;

    public void IncrementTime(float time)
    {
        ElapsedTime += time;
        if (OnIncrementedTime != null)
            OnIncrementedTime(ElapsedTime);
    }

}
