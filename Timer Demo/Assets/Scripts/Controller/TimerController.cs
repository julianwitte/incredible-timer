﻿using UnityEngine;
using System.Collections;

public class TimerController : MonoBehaviour {

    TimerModel m_timer;
    [SerializeField]
    TimerView m_timerView;

    void Start()
    {
        m_timer = new TimerModel();
        m_timerView.SetModel(m_timer);
    }

    public void ActivateTimer()
    {
        StartCoroutine("AdvanceTime");
    }

    public void DeactivateTimer()
    {
        StopCoroutine("AdvanceTime");
    }

    IEnumerator AdvanceTime()
    {
        float lastFrameTime = Time.time;
        while (true)
        {
            m_timer.IncrementTime(Time.time - lastFrameTime);
            lastFrameTime = Time.time;
            yield return 0;
        }
    }

}