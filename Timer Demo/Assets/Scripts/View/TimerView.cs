﻿using UnityEngine;
using UnityEngine.UI;

public class TimerView : MonoBehaviour {

    Text m_text;

	public void SetModel(TimerModel model)
    {
        m_text = GetComponent<Text>();
        model.OnIncrementedTime += SetTime;
    }

    void SetTime(float time)
    {
        m_text.text = string.Format("{0}:{1:00}", (int)time / 60, (int)time % 60); ;
    }
}
